import urllib2
import fal as f

def convert_bitly_urls(input_url): 
    if input_url:
        fp = urllib2.urlopen(input_url)
        return fp.geturl()
    else:
        return None


def model():
    df = ref("hbspt_broadcast_details")
    df = df.to_pandas_on_spark()
    df['social_site_original_url'] = df['social_site_bitly'].apply(convert_bitly_urls)
    df['hubspot_bitly_original_url'] = df['hubspot_bitly'].apply(convert_bitly_urls)
    return df


tdf = model()
f.write_to_model(tdf)
