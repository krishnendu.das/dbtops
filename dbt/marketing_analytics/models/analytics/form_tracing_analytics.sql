-- --matching UTM_CONTENT -174
-- SELECT *
--   -- campaign_title,email_subject, who_am_i,registration_link,attrib_header,	attrib_values	,organisation_name,	campaign_name,hubspot_bitly,	social_site_bitly,messageUrl,content__description
-- FROM
--   `servian-resourcing-dev.servian_analytics.campaign_email_details` a
-- inner JOIN
--   `servian-resourcing-dev.servian_analytics.hbspt_broadcast_details` b
-- ON
--   a.attrib_values = cast (b.redirect_url_content_val as STRING)
--   AND a.attrib_header='utm_content'

-- -- DOESNOT HAVE UTM_CONTENT -455
-- select * THEN attrib_values ELSE NULL  from `servian-resourcing-dev.servian_analytics.campaign_email_details` where  registration_link not like '%utm_content%' 
-- and attrib_header IN ('utm_source','utm_medium')

-- -- non matching UTM_CONTENT -455
{{ config(materialized = 'table') }}
SELECT
  * EXCEPT(registration_link )
FROM
  {{ref('lnkdn_campaign_stats')}} a LEFT JOIN
  --Google Analytics Event 2022 /campaign_group_name/campaign_date
  {{ref('campaign_email_details')}} b -- Google Analytics Event /campaign_title/DATE(submission_time)
  ON a.campaign_date=DATE(b.submission_time)
and
  LOWER(a.campaign_group_name) LIKE CONCAT('%', LOWER(b.campaign_title), '%' )  -- CHANGE THIS JOIN ( USE UTM_CAMPAIGN for JOIN)
  AND utm_campaign IS NOT NULL
  AND ( utm_source IS NOT NULL
    OR utm_term IS NOT NULL
    OR utm_medium IS NOT NULL )
--where campaign_group_name = 'Google Analytics Event 2022'
