{{ config(materialized = 'view') }}

SELECT
  campaignName AS campaign_name,
  DATE(TIMESTAMP_MILLIS (CAST (createdAt AS INT64 ))) AS created_at,
  DATE(TIMESTAMP_MILLIS (CAST (triggerAt AS INT64 ))) AS triggered_at,
  DATE(TIMESTAMP_MILLIS (CAST (finishedAt AS INT64 ))) AS posted_at,
  broadcastGuid AS redirect_url_content_val,
  channelKey,
  content__originalLink AS hubspot_bitly,
  content__link AS social_site_bitly,
  content__fileId,
  content__description,
  content__body,
  content__title,
  messageUrl
 
FROM
    {{ source('marketing_analysis','hubspot_broadcasts') }}  
--WHERE broadcastGuid IN (222783618,224284040)
