{{ config(materialized = 'view') }}

WITH email_campaign_details AS
(
           SELECT     b.id AS email_event,
                      b.email_campaign_id,
                      a.NAME AS a_name,
                      a.app_name,
                      c.property_email,
                      d.conversion_id,
                      d.form_id,
                      d.page_url,
                      d.portal_id,
                      d.title,
                      d.timestamp as submission_time,
                      e.action,
                      e.created_at,
                      e.lead_nurturing_campaign_id,
                      e.NAME AS form_name,
                      e.redirect,
                      e.submit_text
                     
           FROM        {{ source('hubspot','email_campaign') }} a
           INNER JOIN  {{ source('hubspot','email_event') }} b
           ON         b.email_campaign_id=a.id
           INNER JOIN  {{ source('hubspot','contact') }} c
           ON         b.recipient = c.property_email
           INNER JOIN  {{ source('hubspot','contact_form_submission') }} d
           ON         c.id=d.contact_id
           INNER JOIN  {{ source('hubspot','form') }} e
           ON         e.guid = d.form_id ), dedupe AS
(
       SELECT email_campaign_id,
              a_name         AS email_subject,
              form_name as campaign_form_name,
              title          AS campaign_title,
              property_email AS who_am_i,
              page_url       AS registration_link,
              portal_id,
              submission_time,
       FROM   email_campaign_details ), process_url AS
(
       SELECT   campaign_title,
         email_subject ,
         registration_link,
         who_am_i,
         submission_time,
         organisation_name,
         Max(utm_source)   AS utm_source,
         Max(utm_medium)   AS utm_medium,
         Max(utm_term)     AS utm_term,
         Max(utm_content)  AS utm_content,
         Max(utm_campaign) AS utm_campaign
FROM     (
                         SELECT DISTINCT campaign_title,
                                         email_subject,
                                         registration_link,
                                         who_am_i,
                                         submission_time,
                                         CASE
                                                         WHEN Split(url_attributes, '=')[offset(0)] = "utm_source" THEN split(url_attributes, '=')[offset(1)]
                                                         ELSE NULL
                                         END AS utm_source,
                                         CASE
                                                         WHEN split(url_attributes, '=')[offset(0)] = "utm_medium" THEN split(url_attributes, '=')[offset(1)]
                                                         ELSE NULL
                                         END AS utm_medium,
                                         CASE
                                                         WHEN split(url_attributes, '=')[offset(0)] = "utm_term" THEN split(url_attributes, '=')[offset(1)]
                                                         ELSE NULL
                                         END AS utm_term,
                                         CASE
                                                         WHEN split(url_attributes, '=')[offset(0)] = "utm_content" THEN split(url_attributes, '=')[offset(1)]
                                                         ELSE NULL
                                         END AS utm_content,
                                         CASE
                                                         WHEN split(url_attributes, '=')[offset(0)] = "utm_campaign" THEN split(url_attributes, '=')[offset(1)]
                                                         ELSE NULL
                                         END                                                                            AS utm_campaign,
                                         regexp_extract(who_am_i,r'^[a-zA-Z0-9_.+-]+@([a-zA-Z0-9-]+)\.[a-zA-Z0-9-.]+$') AS organisation_name
                         FROM            dedupe a,
                                         unnest(split(regexp_extract(registration_link, r".+\?(.*)"), '&')) AS url_attributes )
GROUP BY campaign_title,
         email_subject ,
         registration_link,
         who_am_i,
         submission_time,
         organisation_name
)
SELECT DISTINCT *
  FROM process_url 