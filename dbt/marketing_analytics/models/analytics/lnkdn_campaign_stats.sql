{{ config(materialized = 'view') }}

with 
all_campaign_details AS
(
         SELECT   a.id   AS campaign_id,
                  a.associated_entity, 
                  a.campaign_group_id,	
                  b.name AS campaign_group_name,
                  a.name as campaign_subgroup_name,
                  a.created_time,
                  a.last_modified_time,
                  a.status,
                  a.version_tag,
                  row_number() over (partition BY a.id ORDER BY a.last_modified_time DESC) AS rank
         FROM    {{source('linkedin_ads','campaign_history')}}  a inner join {{source('linkedin_ads','campaign_group_history')}}  b on 
         a.campaign_group_id = b.id  ), 
campaign_analytics AS
(
       SELECT b.*
       except
                  ( created_time, last_modified_time,campaign_group_id,associated_entity, version_tag, campaign_id ),
                  a.*
       FROM        {{source('linkedin_ads','ad_analytics_by_campaign')}} a
       INNER JOIN all_campaign_details b
       ON         a.campaign_id = b.campaign_id
       WHERE      rank = 1 ), 
click_through_rate AS
(
       SELECT campaign_group_name,
              campaign_subgroup_name, 
              status,
              date(day) as campaign_date,
              coalesce(clicks,0)                                         AS clicks,
              coalesce(impressions,0)                                    AS impressions,
              coalesce(round(clicks / nullif(impressions, 0) * 100,4),0) AS ctr,
              -- COPIED FROM linkedin_ads_campaign_analytics TABLE USED FOR PAID CAMPAING ANALYTICS REPORTING : START --
              coalesce(round(cost_in_local_currency,4),0)                  AS ad_cost_aud,
              coalesce(round(cost_in_local_currency/nullif(clicks,0),4),0) AS average_cpc,
              coalesce(opens,0)                                            AS opens,
              coalesce(opens/nullif(impressions,0),0)                      AS open_rate,
              COALESCE(external_website_conversions,0) AS external_website_conversions,
              coalesce(external_website_post_click_conversions,0) AS external_website_post_click_conversions,
              coalesce(external_website_post_view_conversions,0) AS external_website_post_view_conversions,
              coalesce(external_website_conversions + external_website_post_click_conversions + external_website_post_view_conversions,0) AS total_conversions,
              coalesce(round(((external_website_conversions + external_website_post_click_conversions + external_website_post_view_conversions)/impressions)*100,4),0)       AS conversion_rate,
              coalesce(round(cost_in_local_currency/nullif((external_website_conversions + external_website_post_click_conversions + external_website_post_view_conversions),0),2),0) AS cost_per_conversion
              -- COPIED FROM linkedin_ads_campaign_analytics TABLE USED FOR PAID CAMPAING ANALYTICS REPORTING : END --
       FROM   campaign_analytics )
SELECT campaign_group_name,
       campaign_subgroup_name,
       --status,
       campaign_date,
       round(sum(clicks),0) as total_clicks,
       round(sum(impressions),0) as total_impressions,
       round(sum(ctr),0) as ctr,
       round(sum(ad_cost_aud),0) as total_ad_cost_aud ,
       round(coalesce(round(sum(ad_cost_aud)/nullif(sum(clicks),0),4),0),0)  as cost_per_click,
       round(sum(opens),0) as total_opens,
       round(sum(open_rate),0) as open_rate,
       round(sum(external_website_conversions),0)  as total_external_website_conversions ,
       round(sum(external_website_post_click_conversions),0) as total_external_website_post_click_conversions,
       round(sum(external_website_post_view_conversions),0) as total_external_website_post_view_conversions,
       round(sum(total_conversions),0) as total_conversions,
       round(sum(conversion_rate),0) as conversion_rate,
       round(sum(cost_per_conversion),0) as cost_per_conversion
  FROM click_through_rate
  group by campaign_group_name, campaign_date, campaign_subgroup_name
  ORDER BY campaign_date desc, campaign_group_name
      DESC  